#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>


// you can change
// 
//#define HOST_IP					"192.168.99.130"
#define HOST_IP					"127.0.0.1"
#define PORT_LOW				49193
#define PORT_HIGH				49202
#define UPLOAD_INTERVAL 		30			// seconds 
//#define NO_DATA_NO_UPLOAD


// advice not change
//
#define CONNECT_TRY				5
#define CONNECT_TIMEOUT			5000		// useconds
#define SELECT_TIMEOUT_SECOND	3			// seconds
#define SELECT_TIMEOUT_USECOND	0			// useconds


